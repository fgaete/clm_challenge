//se importa mongoose
var mongoose = require('mongoose');
var app = require('./app');
var port = 3700;

//se inicializa mongoose y realiza conexión con base mongodb
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/challenge_node',{useNewUrlParser: true, useUnifiedTopology: true})
        .then(()=>{
            console.log("Conectado a BD correctamente!");
            // Creamos el servidor
            app.listen(port,() => {
                console.log("Servicio iniciado correctamente!");
            })
        })
        .catch(err=> console.log(err));