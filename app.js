//se importan las dependencias necesarias (koa, bodyparser, router)
const Koa = require('koa');
const router = require('koa-router');
const respond = require('koa-respond');
var bodyParser = require('koa-bodyparser');

//instanciar Koa
const app = new Koa();
//configuración middleware
app.use(bodyParser());

//configurar rutas
app.use(async ctx => {
    ctx.body = {message:'Hello World'};
});

//exportar el módulo
module.exports = app;