const { string, number } = require('joi');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var movieSchema = Schema ({
    title: String,
    year: Number,
    released: Number,
    genre: String,
    director: String,
    actors: String,
    plot: String,
    ratings: Number
});

module.exports = mongoose.model('challenge_node', movieSchema);